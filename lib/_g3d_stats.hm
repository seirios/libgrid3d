%once g3d_stats

%table g3d_stats = (group,offset) %tsv{
	minmaxsum	0
	meanstdev	%table-size(g3d_stats_minmaxsum)
	quantiles	%table-size(g3d_stats_minmaxsum) + %table-size(g3d_stats_meanstdev)
%}

%table g3d_stats_minmaxsum = (name,description) %tsv{
	min		minimum
	max		maximum
	range	max - min
	sum		sum
%}

%table g3d_stats_meanstdev = (name,description) %tsv{
	mean	mean
	stdev	standard deviation
%}

%table g3d_stats_quantiles = (name,q,description) %tsv{
	median	0.50	median
	pctl05	0.05	5th percentile
	pctl10	0.10	10th percentile
%}

%comment Min, max, and sum computation
%snippet g3d_stats_minmaxsum = (iterator,index) %{
    double min = DBL_MAX;
    double max =-DBL_MAX;
    double sum = 0.0;
    double c = 0.0;

    /* Kahan summation in parallel */
#if defined(_OPENMP) && _OPENMP >= 201107
#pragma omp parallel for reduction(max:max) reduction(min:min) reduction(+:sum) reduction(+:c)
#endif
    ${iterator} {
        double val = inter + slope * g3d_get_dvalue(x,${index});
        if(val < min) min = val;
        if(val > max) max = val;
        double y = val - c;
        double t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }
    sum = sum - c;

    stats->min = min;
    stats->max = max;
    stats->range = max - min;
    stats->sum = sum;
%}

%comment Mean and stdev computation
%snippet g3d_stats_meanstdev = (iterator,index) %{
    index_t nvx = 0;
    double mean = 0.0;
    double sd = 0.0;

    ${iterator} {
        double val = inter + slope * g3d_get_dvalue(x,${index});
        double del = val - mean;
        mean += del / (double)++nvx;
        sd += del * (val - mean);
    }
    sd = nvx < 2 ? NAN : sqrt(sd / (double)(nvx - 1));

    stats->mean = mean;
    stats->stdev = sd;
%}

%comment Running quantiles computation
%snippet g3d_stats_quantiles_run = (iterator,index) %{
    %map g3d_stats_quantiles %%{
        gsl_rstat_quantile_workspace *$${name} = gsl_rstat_quantile_alloc($${q});
    %%}

    ${iterator} {
        double val = inter + slope * g3d_get_dvalue(x,${index});
        %map g3d_stats_quantiles %%{
            gsl_rstat_quantile_add(val,$${name});
        %%}
    }

    %map g3d_stats_quantiles %%{
        stats->$${name} = gsl_rstat_quantile_get($${name});
        gsl_rstat_quantile_free($${name});
    %%}
%}
