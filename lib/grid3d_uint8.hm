#ifndef G3D_uint8_h_
#define G3D_uint8_h_

#include "grid3d.h"

/* selection bits */
%table selbits = (bit) %tsv{
	bit
	tmp
%}

%map selbits %{
#define SEL_$U{bit} (1UL << ${#NR})
%}

/* unit increments in X,Y,Z directions */
%table increments = (name,var,incr) %tsv{
	Jm1	j	-1
	Jp1	j	+1
	Im1	i	-1
	Ip1	i	+1
	Km1	k	-1
	Kp1	k	+1
%}

%map [add1=2] increments %{
#define ${name} ${#NR} // ${var} ${incr}
#define NN_${name} (1UL << ${name}) // bit mask
%}

/* convenience bit masks */
#define NN_ALL   (NN_Jm1 | NN_Jp1 | NN_Im1 | NN_Ip1 | NN_Km1 | NN_Kp1)
#define NN_JMASK (NN_Im1 | NN_Ip1 | NN_Km1 | NN_Kp1)
#define NN_IMASK (NN_Jm1 | NN_Jp1 | NN_Km1 | NN_Kp1)
#define NN_KMASK (NN_Jm1 | NN_Jp1 | NN_Im1 | NN_Ip1)

/* bit manipulators */
%map selbits %{
#define    g3d_sel${bit}_set_p(u8p,ix)  (((uint8_t*)(u8p))[ix] |= SEL_$U{bit})
#define  g3d_sel${bit}_check_p(u8p,ix) ((((uint8_t*)(u8p))[ix] >> ${#NR}) & 1UL)
#define  g3d_sel${bit}_clear_p(u8p,ix)  (((uint8_t*)(u8p))[ix] &= ~SEL_$U{bit})
#define g3d_sel${bit}_toggle_p(u8p,ix)  (((uint8_t*)(u8p))[ix] ^= SEL_$U{bit})
#define   g3d_sel${bit}_copy_p(dst_u8p,dst_ix,src_u8p,src_ix)  ((((uint8_t*)(dst_u8p))[dst_ix]) \
         ^= (-(unsigned)g3d_sel${bit}_check_p(src_u8p,src_ix) ^ (((uint8_t*)(dst_u8p))[dst_ix])) & SEL_$U{bit})

#define    g3d_sel${bit}_set(g3d,ix)    g3d_sel${bit}_set_p(g3d_u8_p(g3d),ix)
#define  g3d_sel${bit}_check(g3d,ix)  g3d_sel${bit}_check_p(g3d_u8_p(g3d),ix)
#define  g3d_sel${bit}_clear(g3d,ix)  g3d_sel${bit}_clear_p(g3d_u8_p(g3d),ix)
#define g3d_sel${bit}_toggle(g3d,ix) g3d_sel${bit}_toggle_p(g3d_u8_p(g3d),ix)
#define g3d_sel${bit}_copy(dst_g3d,dst_ix,src_g3d,src_ix) \
    g3d_sel${bit}_copy_p(g3d_u8_p(dst_g3d),dst_ix,g3d_u8_p(src_g3d),src_ix)
%}

#define   g3d_nhood_get_p(u8p,ix)   (((uint8_t*)(u8p))[ix])
#define   g3d_nhood_set_p(u8p,ix,n) (((uint8_t*)(u8p))[ix] |=  (n))
#define g3d_nhood_clear_p(u8p,ix,n) (((uint8_t*)(u8p))[ix] &= ~(n))

#define g3d_nhood_get(g3d,ix)       g3d_nhood_get_p(g3d_u8_p(g3d),ix)
#define g3d_nhood_set(g3d,ix,n)     g3d_nhood_set_p(g3d_u8_p(g3d),ix,n)
#define g3d_nhood_clear(g3d,ix,n) g3d_nhood_clear_p(g3d_u8_p(g3d),ix,n)
#define g3d_nhood_check(n,nn) (((nn) >> (n)) & 1UL)
#define g3d_nhood_mask(p) ((p) == PLANE_XY ? NN_KMASK : ((p) == PLANE_XZ ? NN_IMASK : NN_JMASK))

g3d_ret g3d_u8_setup_nhood(grid3d*,const int);
g3d_ret g3d_u8_selbit_over(grid3d*,const grid3d*);
g3d_ret g3d_u8_selbit_clear(grid3d*);
g3d_ret g3d_u8_selchk_minmax(const grid3d*,const grid3d*,double*,double*);

#endif
