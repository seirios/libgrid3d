%include "grid3d_coord.hm"
#include "grid3d_resamp.h"

#include <gsl/gsl_math.h>

#include <math.h>

%def G3D_COORDF g3d_coord_carpol {
    if(x == NULL) return G3D_ENOINPUT; // no input

    if(x->nz == 1) { // 2D -> plane polar
        double rmax = 0.5 * hypot(x->nx,x->ny);
        double dr = 1.0;
        double dtheta = dr / rmax;
        coord_t nr = ceil(rmax/dr);
        coord_t ntheta = (2.0 * M_PI) / dtheta;
        grid3d *y = g3d_alloc(doble ? G3D_FLOAT64 : G3D_FLOAT32,ntheta,nr,1,true);
        if(y == NULL) return G3D_ENOMEM;
#ifdef _OPENMP
#pragma omp parallel for
#endif
        for(index_t j=0;j<ntheta;j++)
            for(index_t i=0;i<nr;i++) {
                double theta = j*dtheta;
                double r = i*dr;
                g3d_set_dvalue(y,j+i*y->nx,
                        bilinear_interp(x,0.5 * x->nx + r * cos(theta),
                                          0.5 * x->ny - r * sin(theta),fill));
            }
        *y_o = y;
    } else { // 3D -> spherical
        double rmax = 0.5 * gsl_hypot3(x->nx,x->ny,x->nz);
        double dr = 1.0;
        double dtheta = 2.0 * dr / rmax;
        double dphi = 2.0 * dtheta;
        coord_t nr = ceil(rmax/dr);
        coord_t ntheta = (2.0 * M_PI) / dtheta;
        coord_t nphi = M_PI / dphi;
        grid3d *y = g3d_alloc(doble ? G3D_FLOAT64 : G3D_FLOAT32,ntheta,nr,nphi,true);
        if(y == NULL) return G3D_ENOMEM;
#ifdef _OPENMP
#pragma omp parallel for
#endif
        for(index_t j=0;j<ntheta;j++)
            for(index_t i=0;i<nr;i++)
                for(index_t k=0;k<nphi;k++) {
                    double theta = j*dtheta;
                    double r = i*dr;
                    double phi = k*dphi;
                    g3d_set_dvalue(y,j+i*y->nx+k*y->nxy,
                            trilinear_interp(x,0.5 * x->nx + r * sin(phi) * cos(theta),
                                               0.5 * x->ny - r * sin(phi) * sin(theta),
                                               0.5 * x->nz + r * cos(phi),fill));
                }
        *y_o = y;
    }

    return G3D_OK;
}

/* TODO: write function */
%def G3D_COORDF g3d_coord_polcar {
    %unused x,fill,doble,y_o;
    return G3D_FAIL;
}
