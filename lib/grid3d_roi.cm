#include <float.h>
#include <math.h>
#include <zlib.h>

#include "grid3d.h"
#include "grid3d_roi.h"
#include "grid3d_uint8.h"

#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_rstat.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_math.h>

%include "_g3d_stats.hm"
%include "_typeswact.hm"

#define SQR(x) ((x) * (x))
#define X(ix,nx,dim) (((ix)%(dim))%(nx))
#define Y(ix,nx,dim) (((ix)%(dim))/(nx))
#define Z(ix,nx,dim)  ((ix)/(dim))

/* Allocate a g3d_roi */
__attribute__ ((malloc))
g3d_roi* g3d_roi_alloc(const char *const name, const index_t size) {
    g3d_roi *x = calloc(1,sizeof(g3d_roi));
    if(x == NULL) return NULL; // calloc failed

    x->size = size;
    x->vox = (x->size >= 1) ? calloc(3 * x->size,sizeof(coord_t)) : NULL;
    x->name = (name != NULL) ? strdup(name) : strdup("");
    x->nlen = strlen(x->name);

    return x;
}

/* Duplicate a g3d_roi */
__attribute__ ((malloc))
g3d_roi* g3d_roi_dup(const g3d_roi *const x) {
    if(x == NULL) return NULL; // null input
    g3d_roi *y = g3d_roi_alloc(x->name,x->size);
    if(y == NULL) return NULL; // malloc failed
    memcpy(y->vox,x->vox,3 * y->size * sizeof(coord_t));

    return y;
}

/* Test if two g3d_roi are equal */
int g3d_roi_equal(const g3d_roi *const x, const g3d_roi *const y) {
    if(x->size != y->size) return 0;
    if(x->nlen != y->nlen) return 0;
    if(strcmp(x->name,y->name) != 0) return 0;
    return !memcmp(x->vox,y->vox,3 * x->size * sizeof(coord_t));
}

/* Free a g3d_roi */
void _g3d_roi_free(g3d_roi *const x) {
    if(x == NULL) return;

    %free(x->name);
    %free(x->vox);
    free(x); 
}

/* Create a g3d_roi from a grid3d of uint8 type */
/* TODO: parallelize? */
g3d_roi* g3d_roi_new(const grid3d *x, const char *name, const index_t size) {
    if(x == NULL || x->type != G3D_UINT8) return NULL; /* invalid input */
    g3d_roi *y = g3d_roi_alloc(name,size);
    if(y == NULL) return NULL; /* malloc failed */

    index_t nvx = 0;
    for(index_t ix=0;ix<x->nvox;ix++)
        if(g3d_selbit_check(x,ix)) {
            ROI_J(nvx,y) = X(ix,x->nx,x->nxy);
            ROI_I(nvx,y) = Y(ix,x->nx,x->nxy);
            ROI_K(nvx,y) = Z(ix,x->nx,x->nxy);
            if(++nvx == size) break;
        }

    return y;
}

/* Select a g3d_roi onto a grid3d of uint8 type */
g3d_ret g3d_roi_select(const g3d_roi *const roi, grid3d *const sel, const bool Tset_Fclear) {
    if(roi == NULL || sel == NULL) return G3D_ENOINPUT;

#ifdef _OPENMP
#pragma omp parallel for
#endif
    g3d_roi_foreach(roi,ix) {
        if(Tset_Fclear) g3d_selbit_set  (sel,ROI_IX(ix,roi,sel));
        else            g3d_selbit_clear(sel,ROI_IX(ix,roi,sel));
    }

    return G3D_OK;
}

/* Translate a g3d_roi by increments dx,dy,dz */
g3d_ret g3d_roi_translate(g3d_roi *const roi, const int dx, const int dy, const int dz) {
    if(roi == NULL) return G3D_ENOINPUT; // no input
    if(dx == 0 && dy == 0 && dz == 0) return G3D_FAIL; // no translation

#ifdef _OPENMP
#pragma omp parallel for
#endif
    g3d_roi_foreach(roi,ix) {
        ROI_J(ix,roi) += dx;
        ROI_I(ix,roi) += dy;
        ROI_K(ix,roi) += dz;
    }

    return G3D_OK;
}

/* Allocate a g3d_roi_list */
__attribute__ ((malloc))
g3d_roi_list* g3d_roi_list_alloc(const nroi_t size) {
    g3d_roi_list *x = calloc(1,sizeof(g3d_roi_list));
    if(x == NULL) return NULL; // calloc failed

    x->size = size;
    x->rois = calloc(size,sizeof(g3d_roi*));
    if(x->rois == NULL) {
        g3d_roi_list_free(x);
        return NULL; // calloc failed
    }

    return x;
}

/* Free a g3d_roi_list */
void _g3d_roi_list_free(g3d_roi_list *const x) {
    if(x == NULL) return;

    if(x->rois != NULL) {
        for(int i=0;i<x->size;i++)
            g3d_roi_free(x->rois[i]);
        %free(x->rois);
    }
    free(x);
}

/* Load a ROI list from file, within bounds of grid3d x */
/* TODO: check file structure is correct */
g3d_roi_list* g3d_roi_list_load(const char *const file, const grid3d *const x) {
    if(file == NULL || x == NULL) return NULL; // null input
    gzFile zfp = gzopen(file,"rb");
    if(zfp == NULL) return NULL; // gzopen failed

    nroi_t nroi;
    gzfread(&nroi,sizeof(nroi_t),1,zfp); // list size
    g3d_roi_list *list = g3d_roi_list_alloc(nroi);
    if(list == NULL) return NULL; // malloc failed

    nroi_t or = 0;
    for(nroi_t ir=0;ir<nroi;ir++) {
        index_t size;
        uint16_t nlen;

        gzfread(&nlen,sizeof(uint16_t),1,zfp);             // read ROI name length
        char *name = malloc((nlen + 1) * sizeof(char));

        gzfread(name,sizeof(char),nlen,zfp);               // read ROI name
        name[nlen] = '\0';
        gzfread(&size,sizeof(index_t),1,zfp);              // read ROI size

        g3d_roi *roi = g3d_roi_alloc(name,size);
        if(roi == NULL) {
            g3d_roi_list_free(list);
            gzclose(zfp);
            return NULL; // g3d_roi_alloc failed
        }

        gzfread(roi->vox,sizeof(coord_t),3 * roi->size,zfp); // read ROI voxels

        /* copy in-bounds voxels only */
        index_t ik = 0;
        g3d_roi_foreach(roi,ix) {
            if(ROI_J(ix,roi) < x->nx && ROI_I(ix,roi) < x->ny && ROI_K(ix,roi) < x->nz)
                memcpy(&roi->vox[3 * ik++],&roi->vox[3 * ix],3 * sizeof(coord_t));
        }

        if(ik == 0) { // ROI out-of-bounds entirely
            g3d_roi_free(roi); roi = NULL;
        } else { // resize to keep in-bounds voxels only
            roi->size = ik;
            void *tmp = realloc(roi->vox,3 * roi->size * sizeof(coord_t));
            if(tmp == NULL) { // realloc failed
                g3d_roi_free(roi);
                roi = NULL;
            } else {
                roi->vox = tmp;
                list->rois[or++] = roi;
            }
        }
    }

    gzclose(zfp);

    if(or == 0) { // no ROIs in-bounds
        g3d_roi_list_free(list); list = NULL;
    } else {
        list->size = or;
        void *tmp = realloc(list->rois,list->size * sizeof(g3d_roi*)); 
        if(tmp == NULL) { // realloc failed
            g3d_roi_list_free(list);
            list = NULL;
        } else
            list->rois = tmp;
    }

    return list;
}

/* Save a ROI list to file */
g3d_ret g3d_roi_list_save(const char *const file, const g3d_roi_list *const list) {
    if(file == NULL || list == NULL) return G3D_ENOINPUT;
    gzFile zfp = gzopen(file,"wb6"); // level 6 compression
    if(zfp == NULL) return G3D_ENOFILE;

    gzfwrite(&list->size,sizeof(nroi_t),1,zfp); // write list size
    for(nroi_t ir=0;ir<list->size;ir++) {
        g3d_roi *roi = list->rois[ir];
        gzfwrite(&roi->nlen,sizeof(uint16_t),1,zfp);          // write ROI name length
        gzfwrite(roi->name,sizeof(char),roi->nlen,zfp);       // write ROI name
        gzfwrite(&roi->size,sizeof(index_t),1,zfp);           // write ROI size
        gzfwrite(roi->vox,sizeof(coord_t),3 * roi->size,zfp); // write ROI voxels
    }

    gzclose(zfp);

    return G3D_OK;
}

/**
 * Applies a mask to a grid3d, filling out those voxels not in the mask. The mask must
 * be a g3d_roi with voxels within the geometry of the grid3d.
 */
void g3d_roi_mask_dfill(grid3d *const x, const g3d_roi *const roi, const double fill) {
    if(x == NULL || roi == NULL) return; // null input

    %snippet g3d_roi_mask_dfill = (type) %{
        void *tmp = calloc(roi->size,sizeof(${type}));
        if(tmp != NULL) {
            /* save values */
#ifdef _OPENMP
#pragma omp parallel for
#endif 
            g3d_roi_foreach(roi,ix) {
                ((${type}*)tmp)[ix] = ((${type}*)x->dat)[ROI_IX(ix,roi,x)];
            }
            /* replace with fill */
#ifdef _OPENMP
#pragma omp parallel for
#endif
            g3d_foreach(x,ix) {
                ((${type}*)x->dat)[ix] = (${type})fill;
            }
            /* put values back */
#ifdef _OPENMP
#pragma omp parallel for
#endif
            g3d_roi_foreach(roi,ix) {
                ((${type}*)x->dat)[ROI_IX(ix,roi,x)] = ((${type}*)tmp)[ix];
            }
        }
        %free(tmp);
    %}
    %recall TYPE_SWITCH_ACTION (`g3d_roi_mask_dfill`,`x->type`)
}

/* find index of nearest voxel in ROI */
static inline index_t roi_nearest_ix(const g3d_roi roi[const static 1], const index_t ix, const grid3d g3d[const static 1]) {
    const double x = X(ix,g3d->nx,g3d->nxy);
    const double y = Y(ix,g3d->nx,g3d->nxy);
    const double z = Z(ix,g3d->nx,g3d->nxy);

    index_t nearest_ix = ix;
    double d2, min_d2 = HUGE_VAL;
    g3d_roi_foreach(roi,roi_ix) {
        const double roi_x = ROI_J(roi_ix,roi);
        const double roi_y = ROI_I(roi_ix,roi);
        const double roi_z = ROI_K(roi_ix,roi);
        if((d2 = %map [sep=`+`] {x,y,z}(x) %{ SQR(${x} - roi_${x}) %}) < min_d2) {
            nearest_ix = ROI_IX(roi_ix,roi,g3d);
            min_d2 = d2;
        }
    }

#if DEBUG
    fprintf(stderr,"%zu (%g,%g,%g) %g %zu (%g,%g,%g)\n",
            ix,x,y,z,min_d2,nearest_ix,
            X(nearest_ix,g3d->nx,g3d->nxy),
            Y(nearest_ix,g3d->nx,g3d->nxy),
            Z(nearest_ix,g3d->nx,g3d->nxy));
#endif

    return nearest_ix;
}

/* find index of nearest voxel in ROI (in parallel) */
static inline index_t roi_nearest_ix_par(const g3d_roi roi[const static 1], const index_t ix, const grid3d g3d[const static 1]) {
    const double x = X(ix,g3d->nx,g3d->nxy);
    const double y = Y(ix,g3d->nx,g3d->nxy);
    const double z = Z(ix,g3d->nx,g3d->nxy);

    index_t result = ix;
    double result_d2 = HUGE_VAL;

#ifdef _OPENMP
    unsigned int nth = 0; // actually used threads
    unsigned int nth_max = omp_get_max_threads(); // maximum possible threads

    index_t nearest_ix[nth_max];
    double min_d2[nth_max];
    for(size_t i = 0; i < nth_max; ++i) { // initialize
        nearest_ix[i] = ix;
        min_d2[i] = HUGE_VAL;
    }
#pragma omp parallel default(none) shared(nth,g3d,roi,x,y,z,nearest_ix,min_d2)
    { // begin parallel section
        const int tid = omp_get_thread_num(); // thread ID
        if(tid == 0) nth = omp_get_num_threads();
#pragma omp for
#else
    const int tid = 0;
    index_t nearest_ix[1] = { ix };
    double min_d2[1] = { HUGE_VAL };
#endif
    g3d_roi_foreach(roi,roi_ix) {
        const double roi_x = ROI_J(roi_ix,roi);
        const double roi_y = ROI_I(roi_ix,roi);
        const double roi_z = ROI_K(roi_ix,roi);
        double d2;
        if((d2 = %map [sep=`+`] {x,y,z}(x) %{ SQR(${x} - roi_${x}) %}) < min_d2[tid]) {
            nearest_ix[tid] = ROI_IX(roi_ix,roi,g3d);
            min_d2[tid] = d2;
        }
    }
#ifdef _OPENMP
    } // end parallel section

    /* reduce location across threads */
    for(size_t i = 0; i < nth; ++i) {
        if(min_d2[i] < result_d2) {
            result = nearest_ix[i];
            result_d2 = min_d2[i];
        }
    }
#else
    result = nearest_ix[0];
    result_d2 = min_d2[0];
#endif

#if DEBUG
    fprintf(stderr,"%zu (%g,%g,%g) %g %zu (%g,%g,%g)\n",
            ix,x,y,z,result_d2,result,
            X(result,g3d->nx,g3d->nxy),
            Y(result,g3d->nx,g3d->nxy),
            Z(result,g3d->nx,g3d->nxy));
#endif

    return result;
}


void g3d_roi_copynearest(grid3d *const g3d, const g3d_roi *const src, const g3d_roi *const dst) {
    if(g3d == NULL || src == NULL) return; // null input

    %snippet g3d_roi_copynearest = (type) %{
        ((${type}*)g3d->dat)[dst_ix] = ((${type}*)g3d->dat)[cpy_ix];
    %}

    if(dst == NULL) { // destination is full grid
#ifdef _OPENMP
#pragma omp parallel for
#endif
        g3d_foreach(g3d,dst_ix) {
            index_t cpy_ix = roi_nearest_ix(src,dst_ix,g3d);
            if(cpy_ix == dst_ix) continue;
            %recall TYPE_SWITCH_ACTION (`g3d_roi_copynearest`,`g3d->type`)
        }
    } else { // destination is ROI dst
        if(src->size > (size_t)50 * dst->size) { // parallelize inner loop
            g3d_roi_foreach(dst,ix) {
                index_t dst_ix = ROI_IX(ix,dst,g3d);
                index_t cpy_ix = roi_nearest_ix_par(src,dst_ix,g3d);
                if(cpy_ix == dst_ix) continue;
                %recall TYPE_SWITCH_ACTION (`g3d_roi_copynearest`,`g3d->type`)
            }
        } else { // parallelize outer loop
#ifdef _OPENMP
#pragma omp parallel for
#endif
            g3d_roi_foreach(dst,ix) {
                index_t dst_ix = ROI_IX(ix,dst,g3d);
                index_t cpy_ix = roi_nearest_ix(src,dst_ix,g3d);
                if(cpy_ix == dst_ix) continue;
                %recall TYPE_SWITCH_ACTION (`g3d_roi_copynearest`,`g3d->type`)
            }
        }
    }
}

/* Applies a given function to every point in a ROI of a grid3d, op'd as double */
g3d_ret g3d_roi_dapply(grid3d *const x, const g3d_roi *const roi, G3D_DAPPLY_FUNC *const f, const void *const p) {
    if(x == NULL || roi == NULL || f == NULL) return G3D_ENOINPUT; // empty input

#ifdef _OPENMP
#pragma omp parallel for
#endif
    g3d_roi_foreach(roi,ix) {
        index_t ir = ROI_IX(ix,roi,x);
        g3d_set_dvalue(x,ir,f(g3d_get_dvalue(x,ir),p));
    }

    return G3D_OK;
}

/* Extracts the bounds of a g3d_roi */
g3d_ret g3d_roi_get_bounds(const g3d_roi *const roi, coord_t *const jm, coord_t *const jM, coord_t *const im, coord_t *const iM, coord_t *const km, coord_t *const kM) {
    if(roi == NULL) return G3D_FAIL; // null input

    %map {j,i,k} (x) %{
    coord_t ${x}min = COORD_MAX, ${x}max = 0;
    %}

    if(roi->size == 0)
        %map {j,i,k} (x) %{ ${x}min = ${x}max = %} 0;
    else {
#if defined(_OPENMP) && _OPENMP >= 201107
#pragma omp parallel for %map {j,i,k} (x) %{ reduction(min:${x}min) reduction(max:${x}max) %} // reduce min/max
#endif
        g3d_roi_foreach(roi,ix) {
            %map {j,i,k} (x) %{
            {
                coord_t ${x} = ROI_$U{x}(ix,roi);
                if(${x} < ${x}min) ${x}min = ${x};
                if(${x} > ${x}max) ${x}max = ${x};
            }
            %}
        }
    }
    
    %map {j,i,k} (x) %{
        if(${x}m != NULL) *${x}m = ${x}min;
        if(${x}M != NULL) *${x}M = ${x}max;
    %}

    return G3D_OK;
}

/* Extracts the bounding chunk of a g3d_roi within a grid3d */
grid3d* g3d_roi_chunk(const grid3d *const x, const g3d_roi *const roi) {
    coord_t jmin,jmax,imin,imax,kmin,kmax;
    if(g3d_roi_get_bounds(roi,&jmin,&jmax,&imin,&imax,&kmin,&kmax) != G3D_OK)
        return NULL; // g3d_roi_get_bounds failed

    /* bounds check */
    %map {j,i,k}{x,y,z} (x,y) %{
    if(${x}max >= x->n${y}) ${x}max = x->n${y} - 1;
    %}

    return g3d_trim(x,jmin,imin,kmin,jmax - jmin + 1,imax - imin + 1,kmax - kmin + 1);
}

/* Returns calibrated values within a ROI and their sum */
double* g3d_roi_get_dvalues(const grid3d *const x, const g3d_roi *const roi, const double *const calib) {
    if(x == NULL || roi == NULL) return NULL; // null input
    double *y = malloc(roi->size * sizeof(double));
    if(y == NULL) return NULL; // malloc failed

    /* setup calibration */
    double inter = (calib != NULL) ? calib[0] : 0.0;
    double slope = (calib != NULL) ? calib[1] : 1.0;

#ifdef _OPENMP
#pragma omp parallel for
#endif
    g3d_roi_foreach(roi,ix) {
        y[ix] = inter + slope * g3d_get_dvalue(x,ROI_IX(ix,roi,x));
    }

    return y;
}

/* Returns ROI statistics given calibration */
g3d_stats* g3d_roi_get_stats(const grid3d *const x, const g3d_roi *const roi, const double *const calib, const enum g3d_stats_mode modemask) {
    if(x == NULL || roi == NULL) return NULL; // null input
    g3d_stats *stats = calloc(1,sizeof(g3d_stats));
    if(stats == NULL) return NULL; // malloc failed

    /* setup calibration */
    double inter = (calib != NULL) ? calib[0] : 0.0;
    double slope = (calib != NULL) ? calib[1] : 1.0;

    if(modemask & G3D_STATS_MINMAXSUM) {
        %recall g3d_stats_minmaxsum (`g3d_roi_foreach(roi,ix)`,`ROI_IX(ix,roi,x)`)
    }

    if(modemask & G3D_STATS_MEANSTDEV) {
        %recall g3d_stats_meanstdev (`g3d_roi_foreach(roi,ix)`,`ROI_IX(ix,roi,x)`)
    }

    if(modemask & G3D_STATS_QUANTILES) {
        /* if ROI is small, compute from sorted data */
        if(roi->size < 1E7) {
            double *values = g3d_roi_get_dvalues(x,roi,calib);

            gsl_sort(values,1,roi->size);
            %map g3d_stats_quantiles %{
                stats->${name} = gsl_stats_quantile_from_sorted_data(values,1,roi->size,${q});
            %}

            %free(values);
        } else { /* use running statistics */
            %recall g3d_stats_quantiles_run (`g3d_roi_foreach(roi,ix)`,`ROI_IX(ix,roi,x)`)
        }
    }

    return stats;
}
