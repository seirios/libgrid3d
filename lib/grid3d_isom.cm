#include <math.h>
#include <sys/time.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_cblas.h>

#include "grid3d.h"
%include "grid3d_isom.hm"
#include "grid3d_resamp.h"

%include "_typeswact.hm"

/* Perform a geometric reflection of a grid3d */
%def g3d_isom_reflect_exact {
    if(x == NULL) return G3D_ENOINPUT; // null input

    switch(axis) {

        case AXIS_Z: 
            %snippet isom_reflect_exact_xy = (type) %{
                const size_t tmpsize = x->nxy * sizeof(${type});
                const index_t one = x->nxy * (x->nz - 1);
                ${type} *const dat = (${type}*)x->dat;
#ifdef _OPENMP
#pragma omp parallel
                {
#endif
                    void *ptmp = malloc(tmpsize);
#ifdef _OPENMP
#pragma omp for
#endif
                    for(index_t i=0;i<x->nz / 2;i++) {
                        const index_t uuu = 0;
                        const index_t vvv = i * x->nxy;
                        memcpy(ptmp,
                               &dat[uuu + vvv],tmpsize);
                        memcpy(&dat[uuu + vvv],
                               &dat[uuu + one - vvv],tmpsize);
                        memcpy(&dat[uuu + one - vvv],ptmp,tmpsize);
                    }
                    %free(ptmp);
#ifdef _OPENMP
                }
#endif
            %}
            %recall TYPE_SWITCH_ACTION (`isom_reflect_exact_xy`,`x->type`)
            break;

        case AXIS_Y:
            %snippet isom_reflect_exact_xz = (type) %{
                const size_t tmpsize = x->nx * sizeof(${type});
                const index_t one = x->nx * (x->ny - 1);
                ${type} *const dat = (${type}*)x->dat;
#ifdef _OPENMP
#pragma omp parallel
                {
#endif
                    void *ptmp = malloc(tmpsize);
#ifdef _OPENMP
#pragma omp for
#endif
                    for(index_t i=0;i<x->ny / 2;i++) {
                        for(index_t j=0;j<x->nz;j++) {
                            const index_t uuu = j * x->nxy;
                            const index_t vvv = i * x->nx;
                            memcpy(ptmp,
                                   &dat[uuu + vvv],tmpsize);
                            memcpy(&dat[uuu + vvv],
                                   &dat[uuu + one - vvv],tmpsize);
                            memcpy(&dat[uuu + one - vvv],ptmp,tmpsize);
                        }
                    }
                    %free(ptmp);
#ifdef _OPENMP
                }
#endif
            %}
            %recall TYPE_SWITCH_ACTION (`isom_reflect_exact_xz`,`x->type`)
            break;

        case AXIS_X:
            %snippet isom_reflect_exact_zy = (type) %{
                const size_t tmpsize = x->nx * sizeof(${type});
                const index_t one = x->nx - 1;
                ${type} *const dat = (${type}*)x->dat;
#ifdef _OPENMP
#pragma omp parallel
                {
#endif
                    ${type} *ptmp = malloc(tmpsize);
#ifdef _OPENMP
#pragma omp for
#endif
                    for(index_t i=0;i<x->ny * x->nz;i++) {
                        const index_t uuu = i * x->nx;
                        for(index_t vvv=0;vvv<x->nx;vvv++)
                            ptmp[vvv] = dat[uuu + one - vvv];
                        memcpy(&dat[uuu],ptmp,tmpsize);
                    }
                    %free(ptmp);
#ifdef _OPENMP
                }
#endif
            %}
            %recall TYPE_SWITCH_ACTION (`isom_reflect_exact_zy`,`x->type`)
            break;
    }

    return G3D_OK;
}

/* TODO: in-place rotation for cubic data? */
/* Perform an exact rotation of a grid3d */
%def g3d_isom_rotate_exact {
    if(x == NULL) return G3D_ENOINPUT; // null input
    while(theta < 0) { theta += 360; } theta %= 360; /* normalize angle */
    if(theta == 0) return G3D_ENOOP; // no rotation

    %snippet rot = (type,j,i,k) %{
        const ${type} *const x_dat = x->dat;
        ${type} *const tmp_dat = tmp->dat;
#ifdef _OPENMP
#pragma omp parallel for
#endif
        g3d_foreach(x,ix) {
            const coord_t X = (ix % x->nxy) % x->nx;
            const coord_t Y = (ix % x->nxy) / x->nx;
            const coord_t Z = (ix / x->nxy);

            %map {x,y,z} (x) %%{
#if %strstr($b{j},`R$$U{x}`) || %strstr($b{i},`R$$U{x}`) || %strstr($b{k},`R$$U{x}`)
            const coord_t R$$U{x} = x->n$${x} - $$U{x} - 1; // reversed coordinate
#endif
            %%}

            size_t tmp_ix = (size_t)(${j}) + (${i}) * tmp->nx + (${k}) * tmp->nxy;
            tmp_dat[tmp_ix] = x_dat[ix];
        }
    %}

%table axis_x = (theta,x,y,z) %tsv{
	90	x	z	ry
	180	x	ry	rz
	270	x	rz	y
%}

%table axis_y = (theta,x,y,z) %tsv{
	90	rz	y	x
	180	rx	y	rz
	270	z	y	rx
%}

%table axis_z = (theta,x,y,z) %tsv{
	90	y	rx	z
	180	rx	ry	z
	270	ry	x	z
%}

    %map {x,y,z} (x) %{
    size_t n${x} = x->n${x};
    size_t nr${x} = x->n${x};
    %}

    grid3d *tmp = NULL;
    switch(axis) {
        %map {x,y,z} (x) %{
        case AXIS_$U{x}:
            switch(theta) {
                 %map axis_${x} %%{
                    case $${theta}:
                        tmp = g3d_alloc(x->type,.nx=n$${x},.ny=n$${y},.nz=n$${z},.alloc=true);
                        if(tmp == NULL) return G3D_FAIL;
                        %snippet [redef] rot_$${0}_$${theta} = (type) %%%{ %| rot ($$$b{type},$$bU{x},$$bU{y},$$bU{z}) |% %%%}
                        %recall TYPE_SWITCH_ACTION (`rot_$${0}_$${theta}`,`x->type`)
                        break;
                %%}
                default: return G3D_FAIL;
            }
            break;
        %}
    }

    /* tmp contains rotated data */
    %map {x,y,z} (x) %{ x->n${x} = tmp->n${x}; %}
    g3d_setup_dim(x);

    %free(x->dat);
    x->dat = tmp->dat; tmp->dat = NULL; // hand-over data
    %free(tmp);

    return G3D_OK;
}

/* arbitrary-angle rotation with linear interpolation */
%def g3d_isom_rotate_matrix {
    if(argv.x == NULL) return NULL; // no input

    %map {x,y,z} (x) %{
    const coord_t n${x} = argv.x->n${x};
    %}
    const slice_t nxy = argv.x->nxy;

    grid3d *const y = g3d_alloc(argv.doble ? G3D_FLOAT64 : G3D_FLOAT32,nx,ny,nz,.alloc=true);
    if(y == NULL) return NULL; // malloc failed

#ifdef _OPENMP
#pragma omp parallel
    {
#endif
        double *U = malloc((size_t)4 * nxy * sizeof(double));
        /* anchors */
        double *U1 = &U[nxy];
        double *U2 = &U[2 * nxy];
        double *U3 = &U[3 * nxy];

        for(index_t ix=0;ix<nxy;ix++) {
            U [ix] = (ix % nx) + 0.5; // x
            U1[ix] = (ix / nx) + 0.5; // y
            U3[ix] = 1.0; // affine part
        }

        double *V = malloc((size_t)3 * nxy * sizeof(double));
        /* anchors */
        double *V1 = &V[nxy];
        double *V2 = &V[2 * nxy];

        if(argv.doble) {
            %snippet g3d_isom_rotate_matrix = (type) %{
#ifdef _OPENMP
#pragma omp for
#endif
            for(index_t k=0;k<nz;k++) {
                const index_t uuu = k * nxy;
                for(index_t ix=0;ix<nxy;ix++) U2[ix] = k + 0.5; // z
                cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,3,nxy,4,1.0,argv.R->m,4,U,nxy,0.0,V,nxy);
                for(index_t ix=0;ix<nxy;ix++) /* interpolate each value -> slow! */
                    g3d_${type}(y,ix + uuu) = trilinear_interp(argv.x,V[ix],V1[ix],V2[ix],argv.fill);
            }
            %}
            %recall g3d_isom_rotate_matrix (`f64`)
        } else {
            %recall g3d_isom_rotate_matrix (`f32`)
        }

        %free(U); U1 = U2 = U3 = NULL;
        %free(V); V1 = V2 = NULL;
#ifdef _OPENMP
    }
#endif

    return y;
}

%def g3d_isom_rot_matrix {
    double s = sin(theta);
    double c = cos(theta);

#define R(i,j,val) M[j + i * 3] = val;
    switch(axis) {
        case AXIS_X:
            R(0,0, 1); /*  0          0   */
            /*  0   */ R(1,1, c); R(1,2, s);
            /*  0   */ R(2,1,-s); R(2,2, c);
            break;
        case AXIS_Y:
            R(0,0, c); /*  0   */ R(0,2,-s);
            /*  0   */ R(1,1, 1); /*  0   */
            R(2,0, s); /*  0   */ R(2,2, c);
            break;
        case AXIS_Z:
            R(0,0, c); R(0,1,-s); /*  0   */
            R(1,0, s); R(1,1, c); /*  0   */
            /*  0          0   */ R(2,2, 1);
            break;
    }
#undef R
}

/* Returns an affine augmented matrix in row-major order (without last row),
 * the translation part keeps the center of data (if set)
 * R x = Rot * x + b; b = (I - Rot) * cen
 * we could use (x - cen), but matrix loses generality */
%def g3d_isom_get_rotation_matrix {
    %map {x,y,z}{alpha,beta,gamma} (x,y) %{
        double R${x}[9];
        g3d_isom_rot_matrix(argv.${y} * M_PI / 180.0,AXIS_$U{x},R${x});
    %}

    double RyRx[3];
    cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,3,3,3,1.0,Ry,3,Rx,3,0.0,RyRx,3);

    double RzRyRx[9];
    cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,3,3,3,1.0,Rz,3,RyRx,3,0.0,RzRyRx,3);

    g3d_aff_m *const R = calloc(1,sizeof(g3d_aff_m));
    memcpy(&R->m[0],&RzRyRx[0],3 * sizeof(double));
    memcpy(&R->m[4],&RzRyRx[3],3 * sizeof(double));
    memcpy(&R->m[8],&RzRyRx[6],3 * sizeof(double));

    if(argv.center) {
        double cen[3] = {argv.x->nx / 2.0, argv.x->ny / 2.0, argv.x->nz / 2.0};
        /* I - Rot */
        RzRyRx[0] = 1.0 - RzRyRx[0]; RzRyRx[1] =     - RzRyRx[1]; RzRyRx[2] =     - RzRyRx[2];
        RzRyRx[3] =     - RzRyRx[3]; RzRyRx[4] = 1.0 - RzRyRx[4]; RzRyRx[5] =     - RzRyRx[5];
        RzRyRx[6] =     - RzRyRx[6]; RzRyRx[7] =     - RzRyRx[7]; RzRyRx[8] = 1.0 - RzRyRx[8];

        double Rcen[3];
        /* b = (I - Rot) * cen */
        cblas_dgemv(CblasRowMajor,CblasNoTrans,3,3,1.0,RzRyRx,3,cen,1,0.0,Rcen,1);
        R->m[ 3] = Rcen[0];
        R->m[ 7] = Rcen[1];
        R->m[11] = Rcen[2];
    }

    return R;
}

%def g3d_isom_rotate {
    if(argv.x == NULL) return NULL; // no input

    g3d_aff_m *R = g3d_isom_get_rotation_matrix(argv.alpha,argv.beta,argv.gamma,.center=true,argv.x);
    grid3d *const res = g3d_isom_rotate_matrix(argv.x,R,argv.fill,argv.doble);
    %free(R);

    return res;
}
